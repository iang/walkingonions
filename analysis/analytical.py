#!/usr/bin/env python3

# Compute analytical formulas for the bytes used per epoch by the
# various modes

import sympy

A, R_B, R_N, R, logR, C_B, C_N, C, gamma, circ, P_Delta, \
DirAuthConsensusMsg, DirAuthENDIVEDiffMsg, DirAuthGetConsensusMsg, \
DirAuthGetENDIVEDiffMsg, DirAuthUploadDescMsg, DirAuthENDIVEMsg, \
DirAuthGetENDIVEMsg, DirAuthUploadDescMsg, RelayConsensusMsg, \
RelayDescMsg, RelayGetConsensusMsg, RelayGetDescMsg, \
SinglePassCreateCircuitMsgNotLast, SinglePassCreateCircuitMsgLast, \
SinglePassCreatedCircuitCellLast, SinglePassCreatedCircuitCellMiddle, \
SinglePassCreatedCircuitCellFirst, TelescopingCreateCircuitMsg, \
TelescopingCreatedCircuitCell, TelescopingExtendCircuitCell, \
TelescopingExtendedCircuitCell, VanillaCreateCircuitMsg, \
VanillaCreatedCircuitCell, VanillaExtendCircuitCell, \
VanillaExtendedCircuitCell, DirAuthGetConsensusDiffMsg, \
DirAuthConsensusDiffMsg, RelayGetConsensusDiffMsg, \
RelayConsensusDiffMsg \
= sympy.symbols("""
A, R_B, R_N, R, logR, C_B, C_N, C, gamma, circ, P_Delta,
DirAuthConsensusMsg, DirAuthENDIVEDiffMsg, DirAuthGetConsensusMsg,
DirAuthGetENDIVEDiffMsg, DirAuthUploadDescMsg, DirAuthENDIVEMsg,
DirAuthGetENDIVEMsg, DirAuthUploadDescMsg, RelayConsensusMsg,
RelayDescMsg, RelayGetConsensusMsg, RelayGetDescMsg,
SinglePassCreateCircuitMsgNotLast, SinglePassCreateCircuitMsgLast,
SinglePassCreatedCircuitCellLast, SinglePassCreatedCircuitCellMiddle,
SinglePassCreatedCircuitCellFirst, TelescopingCreateCircuitMsg,
TelescopingCreatedCircuitCell, TelescopingExtendCircuitCell,
TelescopingExtendedCircuitCell, VanillaCreateCircuitMsg,
VanillaCreatedCircuitCell, VanillaExtendCircuitCell,
VanillaExtendedCircuitCell, DirAuthGetConsensusDiffMsg,
DirAuthConsensusDiffMsg, RelayGetConsensusDiffMsg,
RelayConsensusDiffMsg
""")

globalsubs = [
    (A , 9),
    (R_N , R - R_B),
    (R_B , 0.010 * R),
    (C_N , C - C_B),
    (C_B , 0.16 * C),
    (circ , gamma * C),
    (gamma , 8.9),
    (C , 2500000*R/6500),
    (P_Delta, 0.019),
]

# The actual sizes in bytes of each message type were logged by
# uncommenting this line in network.py:
    # logging.info("%s size %d", type(self).__name__, sz)

singlepass_merkle_subs = [
    (DirAuthConsensusMsg, 877),
    (DirAuthGetConsensusMsg, 41),
    (DirAuthGetENDIVEMsg, 38),
    (DirAuthGetENDIVEDiffMsg, 42),
    (DirAuthENDIVEDiffMsg, (P_Delta * DirAuthENDIVEMsg).subs(globalsubs)),
    (DirAuthENDIVEMsg, 274 * R),
    (DirAuthUploadDescMsg, 425),
    (RelayConsensusMsg, 873),
    (RelayDescMsg, 415),
    (RelayGetConsensusMsg, 37),
    (RelayGetDescMsg, 32),
    (SinglePassCreateCircuitMsgLast, 187),
    (SinglePassCreateCircuitMsgNotLast, 239),
    (SinglePassCreatedCircuitCellFirst, 1426+82*logR),
    (SinglePassCreatedCircuitCellMiddle, 903+41*logR),
    (SinglePassCreatedCircuitCellLast, 190),
]

singlepass_threshsig_subs = [
    (DirAuthConsensusMsg, 789),
    (DirAuthGetConsensusMsg, 41),
    (DirAuthGetENDIVEMsg, 38),
    (DirAuthGetENDIVEDiffMsg, 42),
    (DirAuthENDIVEDiffMsg, DirAuthENDIVEMsg),
    (DirAuthENDIVEMsg, 348*R),
    (DirAuthUploadDescMsg, 425),
    (RelayConsensusMsg, 784),
    (RelayDescMsg, 415),
    (RelayGetConsensusMsg, 37),
    (RelayGetDescMsg, 32),
    (SinglePassCreateCircuitMsgLast, 187),
    (SinglePassCreateCircuitMsgNotLast, 239),
    (SinglePassCreatedCircuitCellFirst, 1554),
    (SinglePassCreatedCircuitCellMiddle, 969),
    (SinglePassCreatedCircuitCellLast, 190),
]

telescoping_merkle_subs = [
    (DirAuthConsensusMsg, 877),
    (DirAuthGetConsensusMsg, 41),
    (DirAuthGetENDIVEMsg, 38),
    (DirAuthGetENDIVEDiffMsg, 42),
    (DirAuthENDIVEDiffMsg, (P_Delta * DirAuthENDIVEMsg).subs(globalsubs)),
    (DirAuthENDIVEMsg, 234 * R),
    (DirAuthUploadDescMsg, 372),
    (RelayConsensusMsg, 873),
    (RelayGetConsensusMsg, 37),
    (RelayGetDescMsg, 32),
    (RelayDescMsg, 362),
    (TelescopingCreateCircuitMsg, 120),
    (TelescopingCreatedCircuitCell, 179),
    (TelescopingExtendCircuitCell, 122),
    (TelescopingExtendedCircuitCell, 493+41*logR),
]

telescoping_threshsig_subs = [
    (DirAuthConsensusMsg, 789),
    (DirAuthGetConsensusMsg, 41),
    (DirAuthGetENDIVEMsg, 38),
    (DirAuthGetENDIVEDiffMsg, 42),
    (DirAuthENDIVEDiffMsg, DirAuthENDIVEMsg),
    (DirAuthENDIVEMsg, 307*R),
    (DirAuthUploadDescMsg, 372),
    (RelayConsensusMsg, 788),
    (RelayGetConsensusMsg, 37),
    (RelayGetDescMsg, 32),
    (RelayDescMsg, 362),
    (TelescopingCreateCircuitMsg, 120),
    (TelescopingCreatedCircuitCell, 179),
    (TelescopingExtendCircuitCell, 122),
    (TelescopingExtendedCircuitCell, 556),
]

vanilla_subs = [
    (DirAuthConsensusDiffMsg, (P_Delta * DirAuthConsensusMsg).subs(globalsubs)),
    (DirAuthConsensusMsg, RelayConsensusMsg),
    (DirAuthGetConsensusDiffMsg, 45),
    (DirAuthGetConsensusMsg, 41),
    (DirAuthUploadDescMsg, 372),
    (RelayConsensusDiffMsg, (P_Delta * RelayConsensusMsg).subs(globalsubs)),
    (RelayConsensusMsg, 219*R),
    (RelayGetConsensusDiffMsg, 41),
    (RelayGetConsensusMsg, 37),
    (VanillaCreateCircuitMsg, 116),
    (VanillaCreatedCircuitCell, 175),
    (VanillaExtendCircuitCell, 157),
    (VanillaExtendedCircuitCell, 176),
]

# The formulas were output by bytecounts.py

singlepass_totrelay = \
  R_N * ( DirAuthConsensusMsg + DirAuthENDIVEDiffMsg + DirAuthGetConsensusMsg + DirAuthGetENDIVEDiffMsg + A*DirAuthUploadDescMsg ) \
+ R_B * ( DirAuthConsensusMsg + DirAuthENDIVEMsg + DirAuthGetConsensusMsg + DirAuthGetENDIVEMsg + A*DirAuthUploadDescMsg ) \
+ C   * ( RelayConsensusMsg + RelayDescMsg + RelayGetConsensusMsg + RelayGetDescMsg ) \
+ circ * ( 3*SinglePassCreateCircuitMsgNotLast + 2*SinglePassCreateCircuitMsgLast + 2*SinglePassCreatedCircuitCellLast + 2*SinglePassCreatedCircuitCellMiddle + SinglePassCreatedCircuitCellFirst + 20 )

singlepass_totclient = \
  C * ( RelayConsensusMsg + RelayDescMsg + RelayGetConsensusMsg + RelayGetDescMsg ) \
+ circ * ( SinglePassCreateCircuitMsgNotLast + SinglePassCreatedCircuitCellFirst + 4 )

telescoping_totrelay = \
  R_N * ( DirAuthConsensusMsg + DirAuthENDIVEDiffMsg + DirAuthGetConsensusMsg + DirAuthGetENDIVEDiffMsg + A*DirAuthUploadDescMsg ) \
+ R_B * ( DirAuthConsensusMsg + DirAuthENDIVEMsg + DirAuthGetConsensusMsg + DirAuthGetENDIVEMsg + A*DirAuthUploadDescMsg ) \
+ C * ( RelayConsensusMsg + RelayDescMsg + RelayGetConsensusMsg + RelayGetDescMsg ) \
+ circ * ( 5*TelescopingCreateCircuitMsg + 5*TelescopingCreatedCircuitCell + 4*TelescopingExtendCircuitCell + 4*TelescopingExtendedCircuitCell + 52 )

telescoping_totclient = \
  C * ( RelayConsensusMsg + RelayDescMsg + RelayGetConsensusMsg + RelayGetDescMsg ) \
+ circ * ( TelescopingCreateCircuitMsg + TelescopingCreatedCircuitCell + 2*TelescopingExtendCircuitCell + 2*TelescopingExtendedCircuitCell + 20 )

vanilla_totrelay = \
  R_N * ( DirAuthConsensusDiffMsg + DirAuthGetConsensusDiffMsg + A*DirAuthUploadDescMsg ) \
+ R_B * ( DirAuthConsensusMsg + DirAuthGetConsensusMsg + A*DirAuthUploadDescMsg ) \
+ C_N * ( RelayConsensusDiffMsg + RelayGetConsensusDiffMsg ) \
+ C_B * ( RelayConsensusMsg + RelayGetConsensusMsg ) \
+ circ * ( 5*VanillaCreateCircuitMsg + 5*VanillaCreatedCircuitCell + 4*VanillaExtendCircuitCell + 4*VanillaExtendedCircuitCell + 52 )

vanilla_totclient = \
  C_N * ( RelayConsensusDiffMsg + RelayGetConsensusDiffMsg ) \
+ C_B * ( RelayConsensusMsg + RelayGetConsensusMsg ) \
+ circ * ( VanillaCreateCircuitMsg + VanillaCreatedCircuitCell + 2*VanillaExtendCircuitCell + 2*VanillaExtendedCircuitCell + 20 )

# Copy the output into plotdats.py, replacing 'R' by 'x' and 'logR' by
# 'cail(log(x)/log(2))'

print('singlepass_merkle_relay =', (singlepass_totrelay/C).subs(globalsubs).subs(singlepass_merkle_subs).simplify())
print('singlepass_merkle_client =', (singlepass_totclient/C).subs(globalsubs).subs(singlepass_merkle_subs).simplify())
print('singlepass_threshsig_relay =', (singlepass_totrelay/C).subs(globalsubs).subs(singlepass_threshsig_subs).simplify())
print('singlepass_threshsig_client =', (singlepass_totclient/C).subs(globalsubs).subs(singlepass_threshsig_subs).simplify())
print('telescoping_merkle_relay =', (telescoping_totrelay/C).subs(globalsubs).subs(telescoping_merkle_subs).simplify())
print('telescoping_merkle_client =', (telescoping_totclient/C).subs(globalsubs).subs(telescoping_merkle_subs).simplify())
print('telescoping_threshsig_relay =', (telescoping_totrelay/C).subs(globalsubs).subs(telescoping_threshsig_subs).simplify())
print('telescoping_threshsig_client =', (telescoping_totclient/C).subs(globalsubs).subs(telescoping_threshsig_subs).simplify())
print('vanilla_relay =', (vanilla_totrelay/C).subs(globalsubs).subs(vanilla_subs).simplify())
print('vanilla_client =', (vanilla_totclient/C).subs(globalsubs).subs(vanilla_subs).simplify())
